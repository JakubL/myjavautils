import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


/**
 * Utility class for loading files (images) from directory
 */
public class MyFileUtils {
    private MyFileUtils() {
        /* Empty private constructor */
    }

    public static List<File> getAllFilesInDirectory(File directory, String[] formatList) {
        File[] allFiles = directory.listFiles();
        allFiles = (allFiles == null) ? new File[0] : allFiles;
        List<File> files = new ArrayList<>();
        for (File file : allFiles) {
            if (file.isFile()) {
                String filename = file.getAbsolutePath();
                String filenameLowerCase = filename.toLowerCase();
                for (String extension : formatList) {
                    if (filenameLowerCase.endsWith(extension)) {
                        files.add(file);
                        break;
                    }
                }
            }
        }
        return files;
    }

    public static List<File> getAllFilesInDirectoryRecursively(File directory, String[] formatList) {
        boolean allFormats = formatList == null;
        File[] allFiles = directory.listFiles();
        allFiles = (allFiles == null) ? new File[0] : allFiles;
        List<File> files = new ArrayList<>();
        for (File file : allFiles) {
            if (file.isFile()) {
                if (allFormats) {
                    files.add(file);
                } else {
                    String filename = file.getAbsolutePath(); // .replace("\\", "/");
                    String filenameLowerCase = filename.toLowerCase();
                    for (String extension : formatList) {
                        if (filenameLowerCase.endsWith(extension)) {
                            files.add(file);
                            break;
                        }
                    }
                }
            } else if (file.isDirectory()) {
                files.addAll(getAllFilesInDirectoryRecursively(file, formatList));
            } else {
                System.err.println("This isn't a file nor directory:");
                System.err.println(file.getAbsolutePath());
            }
        }
        return files;
    }

    public static List<File> getAllFilesInDirectoryRecursively(File directory) {
        return getAllFilesInDirectoryRecursively(directory, null);
    }

    /**
     * Saves String into file
     */
    public static void saveTextFile(String string, File file) {
        try {
            org.apache.commons.io.FileUtils.writeStringToFile(file, string, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns all the formats (extensions) in given folder
     */
    public static List<String> getFormatsInDirectory(File folder) {
        List<File> listOfFiles = MyFileUtils.getAllFilesInDirectoryRecursively(folder);

        HashSet<String> formats = new HashSet<>();

        for (File f : listOfFiles) {
            String[] splitStr = f.getName().split("\\.");
            if (splitStr.length > 1) formats.add(splitStr[splitStr.length - 1]);
        }
        return new ArrayList<>(formats);
    }

    public static List<String> readFileLinesIntoList(File file) {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static String readFileToString(File file) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
