import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException {
        File f = new File("file.txt");
        MyFileUtils.saveTextFile("\ntext\ndasd\nasdasd\n\n", f);

        List<String> l = MyFileUtils.readFileLinesIntoList(f);
        for (String s : l) {
            System.out.println(s);
        }
        System.out.println("Total length: " + l.size());

        System.out.println(MyFileUtils.readFileToString(f));
        System.out.println("THE END");

        File folder = new File(".");
        System.out.println(MyFileUtils.getFormatsInDirectory(folder));
        System.out.println(MyFileUtils.getAllFilesInDirectoryRecursively(folder));


        BufferedImage tree = ImageIO.read(new File("tree.png"));
        BufferedImage treeRotated = MyImageUtils.rotateImage(tree, 90);
        MyImageUtils.saveImageJPEG(treeRotated, new File("tree-rotated.jpg"));
        BufferedImage cropped = MyImageUtils.cropImgArea(treeRotated, new double[]{0.25, 0.25, 0.75, 0.75});
        MyImageUtils.saveImageJPEG(cropped, new File("tree-rotated-cropped.jpg"));
    }
}
