import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class MyImageUtils {
    private MyImageUtils() {
        /* Empty private constructor */
    }

    public static BufferedImage getBufferedImageFromBase64(String base64Image) throws IOException {
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
        return ImageIO.read(new ByteArrayInputStream(imageBytes));
    }

    public static BufferedImage resizeFixedRatio(BufferedImage image, int maxWidth, int maxHeight) {

        if (maxHeight > image.getHeight() && maxWidth > image.getWidth()) {
            return image; // Do not resize
        }
        double ratioX = (double) maxWidth / image.getWidth();
        double ratioY = (double) maxHeight / image.getHeight();
        double ratio = Math.min(ratioX, ratioY);

        int newWidth = (int) (image.getWidth() * ratio);
        int newHeight = (int) (image.getHeight() * ratio);

        return resize(image, newWidth, newHeight);
    }

    public static BufferedImage resize(BufferedImage image, int newWidth, int newHeight) {

        // creates output image
        BufferedImage outputImage = new BufferedImage(newWidth, newHeight, image.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(image, 0, 0, newWidth, newHeight, null);
        g2d.dispose();

        return outputImage;
    }


    public static void saveImageJPEG(BufferedImage image, File outputFile) throws IOException {
        ImageIO.write(image, "jpg", outputFile);
    }

    public static void saveImageJPEGResized(BufferedImage image, int maxWidth, int maxHeight, File outputFile) throws IOException {
        ImageIO.write(resizeFixedRatio(image, maxWidth, maxHeight), "jpg", outputFile);
    }

    public static BufferedImage rotateImage(BufferedImage src, int rotationAngle) {
        rotationAngle = rotationAngle % 360;
        if (rotationAngle == 0) {
            return src;
        }
        if (rotationAngle != 90 && rotationAngle != 180 && rotationAngle != 270) {
            return null;
        }
        double theta = (Math.PI * 2) / 360 * rotationAngle;
        int width = src.getWidth();
        int height = src.getHeight();
        BufferedImage dest;
        if (rotationAngle == 90 || rotationAngle == 270) {
            dest = new BufferedImage(src.getHeight(), src.getWidth(), src.getType());
        } else {
            dest = new BufferedImage(src.getWidth(), src.getHeight(), src.getType());
        }

        Graphics2D graphics2D = dest.createGraphics();

        if (rotationAngle == 90) {
            graphics2D.translate((height - width) / 2, (height - width) / 2);
            graphics2D.rotate(theta, height / 2, width / 2);
        } else if (rotationAngle == 270) {
            graphics2D.translate((width - height) / 2, (width - height) / 2);
            graphics2D.rotate(theta, height / 2, width / 2);
        } else {
            graphics2D.translate(0, 0);
            graphics2D.rotate(theta, width / 2, height / 2);
        }
        graphics2D.drawRenderedImage(src, null);
        return dest;
    }

    /**
     * Flips given image horizontally
     */
    public static void flip(BufferedImage image) {
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight() / 2; j++) {
                int tmp = image.getRGB(i, j);
                image.setRGB(i, j, image.getRGB(i, image.getHeight() - j - 1));
                image.setRGB(i, image.getHeight() - j - 1, tmp);
            }
        }
    }

    public static BufferedImage cropImgArea(BufferedImage image, double[] cropArea) {
        if (cropArea[0] == 0.0 && cropArea[1] == 0.0 && cropArea[2] == 1.0 && cropArea[3] == 1.0) return image;
        int w = image.getWidth(), h = image.getHeight();
        double x = cropArea[0] * w;
        double y = cropArea[1] * h;
        double cropW = cropArea[2] * w - x;
        double cropH = cropArea[3] * h - y;
        int xInt = (int) (x + 0.5), yInt = (int) (y + 0.5);
        int cropWInt = (int) (cropW + 0.5), cropHInt = (int) (cropH + 0.5);
        if (xInt >= 0 && xInt <= w && yInt >= 0 && yInt <= h) {
            if (xInt + cropWInt > w) cropWInt = w - xInt;
            if (yInt + cropHInt > h) cropHInt = h - yInt;
            if (cropWInt > 0 && cropHInt > 0) return image.getSubimage(xInt, yInt, cropWInt, cropHInt);
        }
        return null;
    }
}
